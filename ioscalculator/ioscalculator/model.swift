//
//  model.swift
// ioscalculatorCalculator
//
//  Created by Md Nazrul Islam on 2021-10-04.
//

import Foundation

class Calculation{
    
    var digitOperation: String
    var sentAllStr: String
    var result:Int = 0
    
    init(a:String){
        
        digitOperation = a
        sentAllStr = a
    
    }
    
    func cal_operation()->Int{
        
        let getArray = Array(sentAllStr)
        var R:Character = "!"
        for p in getArray{
            var Q = 0
            
            if let intValue = p.wholeNumberValue{
                
                Q = intValue
                
                result = swithOperation(by: result , by: Q, by: R)
            
            }else{
                
                R = p
            }
        
            
        }
        
        return result
        
    }
    
    func swithOperation(by a: Int, by b: Int, by c:Character)->Int
    {
        var D = 0
        
        switch c {
        case "+":
            D = a + b
           
        case "-":
            D = a - b
        case "*":
            D = a * b
        case "/":
            D = a / b
            
        default: D = b
            
        }
        return D
        
        
        
    }
    
    
}
