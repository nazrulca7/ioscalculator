//
//  ViewController.swift
//  ioscalculator
//
//  Created by Md Nazrul Islam on 2021-10-05.
//

import UIKit

class ViewController: UIViewController {

  
    @IBOutlet weak var disPlayText1: UITextField!
    
   
  
    @IBOutlet weak var disPlayHistory: UITextField!
    
    
    @IBOutlet weak var ppp: UIButton!
    
    let calValarr : Array<Any> = []
    
    var btnMode = 0
    var btnChanged = 0
    var isHistoryInsert = 0
    var countHistoryLine = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
disPlayText1.isUserInteractionEnabled = false
        disPlayHistory.isUserInteractionEnabled = false
        disPlayHistory.isHidden = true
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btnDisplayStandard(_ sender: Any) {
        if(btnMode == 0){
            
            btnMode = 1
            btnChanged = 1
            disPlayHistory.isHidden = false
        
        }else{
            
            btnMode = 0
            
        }
        
        if(btnMode == 1){
            
            
            (sender as AnyObject).setTitle("Advance-With History", for: .normal)
          
            disPlayHistory.isHidden = true
            
        }else if(btnMode == 0)
        {
            countHistoryLine = 0
            btnChanged = 0
            disPlayHistory.text = ""
            (sender as AnyObject).setTitle("Advance-No History", for: .normal)
            disPlayHistory.isHidden = false
            
        }
        
        
    }
    @IBAction func btn1D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "1"
        }
      
       
    }
    @IBAction func btn2D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "2"
        }
    }
    @IBAction func btn3D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "3"
        }
    }
    @IBAction func btn4D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "4"
        }
        
    }
    @IBAction func btn5D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "5"
        }
    }
    @IBAction func btn6D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "6"
        }
    }
    @IBAction func btn7D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "7"
        }
    }
    @IBAction func btn8D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "8"
        }
    }
    @IBAction func btn9D(_ sender: Any) {
        if(checkDigit()){
        disPlayText1.text = disPlayText1.text! + "9"
        }
    }
    
    @IBAction func btn0D(_ sender: Any) {
        if(checkDigit()){
            
             
        
        disPlayText1.text = disPlayText1.text! + "0"
        }
    }
    
    @IBAction func clearBtn(_ sender: Any) {
        
        disPlayText1.text!  = ""
    }
    
     
    @IBAction func btnPD(_ sender: Any) {
        if(checkDigitinEqual()){
        disPlayText1.text = disPlayText1.text! + "+"
        }
    }
    
    @IBAction func btnMD(_ sender: Any) {
        if(checkDigitinEqual()){
        disPlayText1.text = disPlayText1.text! + "-"
        }
    }
    
    @IBAction func btnMulD(_ sender: Any){
        if(checkDigitinEqual()){
        disPlayText1.text = disPlayText1.text! + "*"
        }
    }
    @IBAction func btnDIVD(_ sender: Any) {
        if(checkDigitinEqual()){
        disPlayText1.text = disPlayText1.text! + "/"
        }
    }
    
    @IBAction func btnEquD(_ sender: Any) {
        
        if(checkDigitinEqual()){
            isHistoryInsert = 0
        disPlayText1.text = disPlayText1.text! + "="
        let str = disPlayText1.text!
        let getCal =  Calculation(a: str)
        let getAfterCal = getCal.cal_operation()
        disPlayText1.text = disPlayText1.text! + "\(getAfterCal)"
        
     
        
        }
        if(btnChanged==0 && isHistoryInsert == 0){
            isHistoryInsert = 1
            disPlayHistory.text = disPlayHistory.text! + "\n" + disPlayText1.text!
            let myString = disPlayHistory.text!
            let trimmedString = myString.components(separatedBy: .newlines).joined()
            disPlayHistory.text = trimmedString
        }
    }
    
    
    func checkDigit() -> Bool
        {
            let str = disPlayText1.text!
            let com1 = str.components(separatedBy: ["+","*","/","-"])
            print(com1[com1.count-1])
            let lastNumber = com1[com1.count-1].count
            
           
            print(lastNumber)
            if ( lastNumber>0){
                
                
                return false
            }else{
               
                if(checkDigitinEqual()){
                return true
                }else{
                    
                    return false
                }
            }
            
            
        }
    
    func checkDigitinEqual() -> Bool
    {
        let str = disPlayText1.text!
        let com2 = str.components(separatedBy: ["="])
        
        let lastNumberequal = com2.count
        
       
        print(lastNumberequal)
        if ( lastNumberequal>1){
         
            return false
        
        }else{
            
            return true
        }
        
    }
    
    func concat<T1, T2>(a: T1, b: T2) -> String {
        return "\(a)" + "\(b)"
    }


}

